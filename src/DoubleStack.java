import java.util.LinkedList;

public class DoubleStack {
	private LinkedList<Double> magasin;

	// Meetodite valikul kasutasin abivahendina lehte:
	// http://www.tutorialspoint.com/java/java_linkedlist_class.htm

	public static void main(String[] argum) {
//		DoubleStack m1 = new DoubleStack();
//		DoubleStack m2 = new DoubleStack();
//		System.out.println(m1.equals(m2));
//		m1.push(1.);
//		m2.push(1.);
//		System.out.println(m1.equals(m2));
//		m1.push(0.);
//		m2.push(3.);
//		System.out.println(m1.equals(m2));
	}

	// Constructor
	DoubleStack() {
		magasin = new LinkedList<Double>();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		
		DoubleStack result = new DoubleStack();
		
		for (int i = 0; i < magasin.size(); i++){
			result.push(magasin.get(i));
		}
		
		return result; // TODO!!! Your code here!
	}

	public boolean stEmpty() {
		return magasin.isEmpty();
	}

	public void push(double a) {
		magasin.add(a);
	}

	public double pop() {
		if (magasin.size() < 1) {
			throw new RuntimeException("Magasinis pole piisavalt arve, et teostada pop'i.");
		} else {
			return magasin.removeLast();
		}
	}

	public void op(String s) {
		if (magasin.size() < 2) {
			throw new RuntimeException("Magasinis pole piisavalt arve, et teostada järgnevat op'i: " + s);
		} else {
			
			Double arg1 = magasin.pop();
			Double arg2 = magasin.pop();
			Double result;
	
			if (s.equals("+")) {
				result = arg1 + arg2;
				magasin.push(result);
			} else if (s.equals("-")) {
				result = arg1 - arg2;
				magasin.push(result);
			} else if (s.equals("*")) {
				result = arg1 * arg2;
				magasin.push(result);
			} else if (s.equals("/")) {
				result = arg1 / arg2;
				magasin.push(result);
			} else {
				throw new RuntimeException(s + " pole toetatud operatsioonide hulgas. Toetatud on + - * /");
			}
		}

	}

	public double tos() {
		if (magasin.size() > 0) {
			return magasin.getLast();
		} else {
			throw new RuntimeException("Magasinis pole piisavalt arve, et teostada tos'i.");
		}
		
	}

	@Override
	public boolean equals(Object o) {
		DoubleStack oMagasin = (DoubleStack) o;

		// ...muidu on 1., 0. ja 1. võrdsed.
		if (this.magasin.size() != oMagasin.magasin.size()){
			return false;
		}
		
		// Võrdleme objektide LnkedListide väärtusi vastavalt nende asukohale. Kui kõik klapivad, on DoubleStackid võrdsed.
		for (int i = 0; i < magasin.size(); i++){
			if (!(this.magasin.get(i).equals(oMagasin.magasin.get(i)))){
				return false;
			}
		}		
		return true;
	}

	@Override
	public String toString() {
		String result;
		
		result = magasin.toString();
		
		return result;
	}

	public static double interpret(String pol) {
		double result = 0.;
		int operationCounter = 0;
		String[] split;
		DoubleStack stack = new DoubleStack();
		
		
		// Kui stringis pole midagi -> pole tehet, mida teha.
		if (pol.isEmpty()){
			throw new RuntimeException("Tehte string on tühi.");
		}
		
		// Asendame kõik peale numbritega, operaatoritega ning \'ga algavate juhtude tühikuga ja kaotame whitespace'i.
		pol = pol.replaceAll("[^0-9+*/\\-]+", " ").trim();
				
		// Lõhume töödeldava stringi massiiviks tühikute juurest.
		split = pol.split(" ");
		
		for (int i = 0; i < split.length; i++){
			// Kui tegu on double-tüüpi numbriga, lisame selle magasini.
			try {
				stack.push(Double.parseDouble(split[i]));
			} 
			
			// Ebaõnnestumisel on järelikult tegu operaatoriga (kõik muu võimalik on asendatud) ning võime teostada tehte.
			catch (Exception exception){
				double arg2 = stack.pop();
				double arg1 = stack.pop();
				operationCounter = operationCounter + 1;
				
				if (split[i].equals("+")){
					stack.push(arg1 + arg2);
				} else if (split[i].equals("-")){
					stack.push(arg1 - arg2);
				} else if (split[i].equals("*")){
					stack.push(arg1 * arg2);
				} else if (split[i].equals("/")){
					stack.push(arg1 / arg2);
				} else {
					throw new RuntimeException("Sisendstringi " + pol + " tehe " + arg1 + split [i] + arg2 + " ei õnnestunud. See oli " + operationCounter + ". tehe");
				}
			}
			
		}
				
		result = stack.pop();
		
		// Kontrollime, ega magasini rohkem liikmeid ei jää.
		if (!stack.stEmpty()){
			throw new RuntimeException("Magasin on balansist väljas järgneva sisendstringi puhul: " + pol);
		}
		
		return result;
	}

}
